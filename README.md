# Colruyt price barometer

## GET Requests
Information backend can be found at: http://data.dobit.olen/Projects/PR2011045/001_004/01-ProjectDoc/02-Development/06-Software/prijsbaro_json/prijsbaron.md

## Url parameters
Following parameters should be added to the URL:
- id: The id of the Colruyt branch
- taal: The language. The possibilities are nl, de or fr.
- dagen_geldig: The amount of days to take into consideration when calculating the comparison.
- JSONURL: The url location of the JSON data.

Example: http://domain.com/prijsbaron/?id=3692&taal=nl&dagen_geldig=9000&JSONURL=http:%2F%2Fwamp.dobit.olen:88%2Fprijsbaro