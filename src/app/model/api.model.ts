import { Compare } from './competitor-list.model';

export interface Api {
    responseCode: string;
    compare: Compare;
}
