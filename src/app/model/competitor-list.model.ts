
export interface Compare {
    branch: {
        active: boolean,
        branchId: string,
        name: string
    };
    comparisonDetail: {
        competitor: Competitor[];
        publicationDate: {
            date: string;
        }
    };
}

export interface Competitor {
    competitorDetail: {
        cheapestProduct: boolean,
        colour: string,
        compareDate: string,
        meatProduct: boolean,
        nationalBrands: boolean,
        percentage: number,
        product: string,
        vegetablesAndFruitProduct: boolean
    };
    competitorId: string;
    logicalOrder: number;
    name: string;
    location: string;
}