import { animate, group, keyframes, query, style, transition, trigger } from '@angular/animations';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Compare, Competitor } from 'src/app/model/competitor-list.model';
import { BarometerService } from 'src/app/services/barometer.service';
// tslint:disable-next-line: one-variable-per-declaration
declare var window: { ScalaDone: () => void; };

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss'],
  animations: [
    trigger('state', [
      transition('in => out', [
        group([
          query('.other-branch.barometer', [
            group([
              animate('.8s', keyframes([
                style({ opacity: 1 }),
                style({ opacity: 0 })
              ]))
            ])
          ], { optional: true }),
          query('.footer-text', [
            group([
              animate('.8s', keyframes([
                style({ opacity: 1 }),
                style({ opacity: 0 })
              ]))
            ])
          ], { optional: true })
        ]),
      ]),
      transition('out => in', [
        group([
          query('.other-branch.barometer', [
            group([
              animate('.8s', keyframes([
                style({ opacity: 0 }),
                style({ opacity: 1 })
              ]))
            ])
          ], { optional: true }),
          query('.footer-text', [
            group([
              animate('.8s', keyframes([
                style({ opacity: 0 }),
                style({ opacity: 1 })
              ]))
            ])
          ], { optional: true }),
          query('.other-branch.comparison', [
            group([
              animate('.8s', keyframes([
                style({ opacity: 0 }),
                style({ opacity: 1 })
              ])),
              animate('.5s', keyframes([
                style({ marginLeft: '7vw' }),
                style({ marginLeft: '0vw' })
              ]))
            ])
          ], { optional: true })
        ]),
      ]),
      transition('* => in', [
        group([
          query('.container', [
            group([
              animate('.8s', keyframes([
                style({ opacity: 0 }),
                style({ opacity: 1 })
              ]))
            ])
          ], { optional: true }),
          query('.other-branch.comparison', [
            group([
              animate('.5s', keyframes([
                style({ marginLeft: '7vw' }),
                style({ marginLeft: '0vw' })
              ]))
            ])
          ], { optional: true })
        ]),
      ])
    ]),
  ]
})
export class TemplateComponent implements OnInit, OnDestroy {
  language: string;
  daysValid: string;
  branchId: string;
  jsonUrl: string;
  showNoData: boolean;
  competitorList$: Observable<Compare>;
  destroy$: Subject<boolean> = new Subject<boolean>();
  competitors: Competitor[];
  branchName: string;
  activeCompetitor: Competitor;
  index = 0;
  isLoading: boolean;
  animationState: 'in' | 'out';

  constructor(
    private translate: TranslateService,
    private route: ActivatedRoute,
    private barometerService: BarometerService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.queryParams.pipe(takeUntil(this.destroy$)).subscribe(params => {
      if (params.taal) {
        this.language = params.taal;
      } else {
        // default value
        this.language = 'nl';
      }
      if (params.dagen_geldig) {
        this.daysValid = params.dagen_geldig;
      } else {
        // default value
        this.daysValid = '30';
      }
      if (params.id) {
        this.branchId = params.id;
      }
      if (params.JSONURL) {
        this.jsonUrl = params.JSONURL;
      }
      if (this.language && this.daysValid && this.branchId && this.jsonUrl) {
        // tslint:disable-next-line: max-line-length
        this.barometerService.getPublicationDateList(this.jsonUrl, this.branchId, this.language, this.daysValid).pipe(takeUntil(this.destroy$)).subscribe(publicationDate => {
          if (publicationDate === undefined) {
            this.isLoading = false;
          }
          // tslint:disable-next-line: max-line-length
          this.barometerService.getCompetitorListByBranchAndPublicationDate(this.jsonUrl, this.branchId, this.language, publicationDate).pipe(takeUntil(this.destroy$)).subscribe(competitorList => {
            this.isLoading = false;
            this.branchName = competitorList.branch.name;
            this.competitors = competitorList.comparisonDetail.competitor;
            this.setCompetitorInterval(this.competitors);
            if (this.branchName === undefined || this.competitors === undefined || this.competitors.length <= 0) {
              this.animationState = 'in';
              this.showNoData = true;
            } else {
              this.showNoData = false;
            }
          });
        });
      } else {
        this.animationState = 'in';
        this.showNoData = true;
      }
      this.translate.setDefaultLang(this.language);
    });
  }

  setCompetitorInterval(competitors: Competitor[]): void {
    this.animationState = 'in';
    this.setActiveCompetitor(competitors);
    setInterval(() => {
      this.animationState = 'out';
    }, 7000);
  }

  setActiveCompetitor(competitors: Competitor[]): void {
    if (this.index === competitors.length) {
      try {
        // tslint:disable-next-line: no-eval
        window.ScalaDone();
      } catch (error) {
        console.error(error);
      }
    }
    this.activeCompetitor = competitors[this.index % competitors.length];
    this.index++;
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  afterAnimation(): void {
    if (this.animationState === 'out') {
      console.log('in');
      this.setActiveCompetitor(this.competitors);
      this.changeDetectorRef.detectChanges();
      this.animationState = 'in';
    }
  }
}
