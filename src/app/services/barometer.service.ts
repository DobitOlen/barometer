import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Compare } from '../model/competitor-list.model';
import { Api } from '../model/api.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BarometerService {

  constructor(private http: HttpClient) { }

  getPublicationDateList(jsonUrl: string, branch: string, language: string, daysValid: string): Observable<string> {

    const url = `${jsonUrl}/getPublicationDateList?id=${branch}&language=${language}`;
    return this.http.get<any>(url).pipe(
      map(response => {
        if (response.responseCode !== '200') {
          return undefined;
        }
        let mostRecentDate: string;
        const today = new Date();
        today.setTime(today.getTime() - Number(daysValid) * 24 * 60 * 60 * 1000);
        const substractedDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        response.publication.publicationDate.forEach(publicationDate => {
          if (!mostRecentDate) {
            mostRecentDate = publicationDate.date;
          }
          if (publicationDate.date > mostRecentDate) {
            mostRecentDate = publicationDate.date;
          }
        });
        if (substractedDate > mostRecentDate) {
          return undefined;
        }
        return mostRecentDate;
      })
    );
  }

  getCompetitorListByBranchAndPublicationDate(jsonUrl: string, branch: string, language: string, mostRecentDate: string): Observable<Compare> {
    if (mostRecentDate === undefined || branch === undefined || (language !== 'nl' && language !== 'fr' && language !== 'de')) {
      return undefined;
    }
    const url = `${jsonUrl}/getCompetitorListByBranchAndPublicationDate?id=${branch}&language=${language}&date=${mostRecentDate}`;
    return this.http.get<Api>(url).pipe(
      map(response => {
        if (response.responseCode !== '200') {
          return undefined;
        }
        let branchName = response.compare.branch.name;
        branchName  = branchName.replace(' (COLRUYT)', '');
        let competitors = response.compare.comparisonDetail.competitor;
        for (const competitor of competitors) {
          for (const competitorWithLocation of environment.competitorsWithLocation) {
            if (competitor.name.toLowerCase().includes(competitorWithLocation)) {
              let competitorInfos = competitor.name.split(" ");
              let carrefourIndex = competitorInfos.findIndex(word =>
                word.toLowerCase() === competitorWithLocation
              )
              competitor.name = competitorInfos[carrefourIndex];
              competitorInfos.splice(carrefourIndex, 1);
              competitor.location = competitorInfos.join(" ");
            }  
          }
        }
        const compare: Compare = {
          branch: response.compare.branch,
          comparisonDetail: response.compare.comparisonDetail
        };
        compare.branch.name = branchName;
        compare.comparisonDetail.competitor = competitors;
        return compare;
      }),
    );
  }
}
